CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Developers
 * Maintainers

INTRODUCTION
------------
This project adds an additional assignee field to the revision table of entity types.
This way, you can assign a revision to a specific user to check / or rework the revision.

REQUIREMENTS
------------
Drupal 8 or 9

INSTALLATION
------------
Enable the module

CONFIGURATION
------------
No configuration needed.

DEVELOPERS
----------
* Maarten Heip - https://www.drupal.org/u/mheip

But a special thanks to Intracto, for allowing me to do this on company time.
https://www.intracto.com

MAINTAINERS
-----------
Current maintainers:
  * Maarten Heip - https://www.drupal.org/u/mheip
