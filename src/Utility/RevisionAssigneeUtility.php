<?php

namespace Drupal\revision_assignee\Utility;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * The utility class that contains functions used accross the module.
 *
 * @package Drupal\revision_assignee\Utility
 */
class RevisionAssigneeUtility {

  /**
   * {@inheritdoc}
   */
  public function isEntityTypeSupported(EntityTypeInterface $entity_type) {
    if (!$entity_type instanceof ContentEntityTypeInterface) {
      return FALSE;
    }

    // If the log message is not enabled, do not add the field.
    // todo maybe make the field configurable...
    $logMessage = $entity_type->getRevisionMetadataKey('revision_log_message');

    if (empty($logMessage)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the revision assignee field definition.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The revision assignee field definition.
   */
  public function getRevisionAssigneeFieldDefinition(): BaseFieldDefinition {
    return BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Assign revision to:'))
      ->setDescription(t('The user ID of the assignee of the current revision.'))
      ->setSetting('target_type', 'user')
      ->setRequired(FALSE)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ]);
  }

}
