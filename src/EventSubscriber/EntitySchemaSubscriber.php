<?php

namespace Drupal\revision_assignee\EventSubscriber;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Entity\EntityTypeEventSubscriberTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeListenerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\revision_assignee\Utility\RevisionAssigneeUtility;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines a class for listening to entity schema changes.
 */
class EntitySchemaSubscriber implements EntityTypeListenerInterface, EventSubscriberInterface {

  use EntityTypeEventSubscriberTrait;
  use StringTranslationTrait;

  /**
   * The definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * The last installed schema definitions.
   *
   * @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface
   */
  protected $entityLastInstalledSchemaRepository;

  /**
   * The revision assignee utility.
   *
   * @var \Drupal\revision_assignee\Utility\RevisionAssigneeUtility
   */
  private RevisionAssigneeUtility $revisionAssigneeUtility;

  /**
   * Constructs a new EntitySchemaSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager
   *   Definition update manager.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entityLastInstalledSchemaRepository
   *   Last definitions.
   * @param \Drupal\revision_assignee\Utility\RevisionAssigneeUtility $revisionAssigneeUtility
   *   The revision assignee utility.
   */
  public function __construct(
    EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager,
    EntityLastInstalledSchemaRepositoryInterface $entityLastInstalledSchemaRepository,
    RevisionAssigneeUtility $revisionAssigneeUtility
  ) {
    $this->entityDefinitionUpdateManager = $entityDefinitionUpdateManager;
    $this->entityLastInstalledSchemaRepository = $entityLastInstalledSchemaRepository;
    $this->revisionAssigneeUtility = $revisionAssigneeUtility;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return static::getEntityTypeEvents();
  }

  /**
   * {@inheritdoc}
   */
  public function onEntityTypeCreate(EntityTypeInterface $entity_type) {
    if ($this->revisionAssigneeUtility->isEntityTypeSupported($entity_type)) {
      $this->addRevisionMetadataField($entity_type);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onFieldableEntityTypeCreate(EntityTypeInterface $entity_type, array $field_storage_definitions) {
    $this->onEntityTypeCreate($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function onEntityTypeUpdate(EntityTypeInterface $entity_type, EntityTypeInterface $original) {
    if ($this->revisionAssigneeUtility->isEntityTypeSupported($entity_type) && !$this->revisionAssigneeUtility->isEntityTypeSupported($original)) {
      $this->addRevisionMetadataField($entity_type);
    }

    // If the entity type is no longer supported by revision assignee, remove the
    // revision metadata field.
    if ($this->revisionAssigneeUtility->isEntityTypeSupported($original) && !$this->revisionAssigneeUtility->isEntityTypeSupported($entity_type)) {
      $revision_metadata_keys = $original->get('revision_metadata_keys');
      $field_storage_definition = $this->entityLastInstalledSchemaRepository->getLastInstalledFieldStorageDefinitions($entity_type->id())[$revision_metadata_keys['revision_assignee']];
      $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($field_storage_definition);

      // We are only removing a revision metadata key so we don't need to go
      // through the entity update process.
      $entity_type->setRevisionMetadataKey('revision_assignee', NULL);
      $this->entityLastInstalledSchemaRepository->setLastInstalledDefinition($entity_type);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onFieldableEntityTypeUpdate(EntityTypeInterface $entity_type, EntityTypeInterface $original, array $field_storage_definitions, array $original_field_storage_definitions, array &$sandbox = NULL) {
    $this->onEntityTypeUpdate($entity_type, $original);
  }

  /**
   * {@inheritdoc}
   */
  public function onEntityTypeDelete(EntityTypeInterface $entity_type) {
    // Nothing to do here.
  }

  /**
   * Adds the 'revision_assignee' revision metadata field to an entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type that has been installed or updated.
   */
  protected function addRevisionMetadataField(EntityTypeInterface $entity_type) {
    if (!$entity_type->hasRevisionMetadataKey('revision_assignee')) {
      // Bail out if there's an existing field called 'revision_assignee'.
      if ($this->entityDefinitionUpdateManager->getFieldStorageDefinition('revision_assignee', $entity_type->id())) {
        throw new \RuntimeException("An existing 'revision_assignee' field was found for the '{$entity_type->id()}' entity type. Set the 'revision_assignee' revision metadata key to use a different field name and run this update function again.");
      }

      // We are only adding a revision metadata key so we don't need to go
      // through the entity update process.
      $entity_type->setRevisionMetadataKey('revision_assignee', 'revision_assignee');
      $this->entityLastInstalledSchemaRepository->setLastInstalledDefinition($entity_type);
    }

    $this->entityDefinitionUpdateManager->installFieldStorageDefinition(
      $entity_type->getRevisionMetadataKey('revision_assignee'),
      $entity_type->id(),
      'revision_assignee',
      $this->revisionAssigneeUtility->getRevisionAssigneeFieldDefinition()
    );
  }

}
